package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/user"
	"time"

	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/kube"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/tools/clientcmd"
)

func install(helmInstall *action.Install, chart chart.Chart, forever chan struct{}, cancel context.CancelFunc) time.Duration {
	log.Println("INSTALLING HELM ", chart.Name())
	start := time.Now()
	time.Sleep(5 * time.Second)
	rel, err := helmInstall.Run(&chart, nil)
	if err != nil {
		panic(err)
	}
	elapsed := time.Since(start)
	log.Println("Successfully installed release: ", rel.Name)
	log.Println("Install time: ", elapsed)
	cancel()
	log.Println("FINISHED INSTALLING HELM ", chart.Name())
	return elapsed
}

func uninstall(helmUninstall *action.Uninstall, releaseName string, forever chan struct{}) time.Duration {
	log.Println("UNINSTALLING HELM ", releaseName)
	start := time.Now()
	time.Sleep(1 * time.Second)
	_, err := helmUninstall.Run(releaseName)
	if err != nil {
		log.Println(err)
		//panic(err)
	}
	elapsed := time.Since(start)
	log.Println("Successfully uninstalled release: ", releaseName)
	log.Println("Uninstall time: ", elapsed)
	log.Println("FINISHED UNINSTALLING HELM ", releaseName)
	return elapsed
}

func xd(ctx context.Context, forever chan struct{}) {
	for {
		select {
		case <-ctx.Done(): // if cancel() execute
			forever <- struct{}{}
			return
		default:
			fmt.Println("working")
			time.Sleep(1 * time.Second)
		}
	}
}

func exec(helmUninstall *action.Uninstall, helmInstall *action.Install, chart *chart.Chart, releaseName string) {
	fmt.Println("#################################################################")
	forever := make(chan struct{})
	ctx, cancel := context.WithCancel(context.Background())
	uninstall(helmUninstall, helmInstall.ReleaseName, forever)
	go install(helmInstall, *chart, forever, cancel)
	go xd(ctx, forever)
	<-forever
}

func main() {
	log.Println("Hello world!")
	rules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeconfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rules, &clientcmd.ConfigOverrides{})
	config, err := kubeconfig.ClientConfig()
	if err != nil {
		panic(err)
	}
	clientset := kubernetes.NewForConfigOrDie(config)
	nodeList, err := clientset.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	for _, n := range nodeList.Items {
		log.Print(n.Name)
	}

	currentUser, err := user.Current()
	if err != nil {
		panic(err)
	}

	chartPath := "/home/" + currentUser.Username + "/ji/spotipi-0.1.0.tgz"
	chart, err := loader.Load(chartPath)
	if err != nil {
		panic(err)
	}

	kubeconfigPath := "/home/" + currentUser.Username + "/.kube/config"
	releaseName := "spotipi"
	releaseNamespace := "spotipi"
	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kube.GetConfig(kubeconfigPath, "", releaseNamespace), releaseNamespace, os.Getenv("HELM_DRIVER"), func(format string, v ...interface{}) {
		log.Printf(format, v)
	}); err != nil {
		panic(err)
	}

	helmInstall := action.NewInstall(actionConfig)
	helmInstall.Namespace = releaseNamespace
	helmInstall.ReleaseName = releaseName

	helmUninstall := action.NewUninstall(actionConfig)
	helmUninstall.Timeout = time.Duration(3600)
	helmUninstall.Wait = true

	exec(helmUninstall, helmInstall, chart, releaseName)
	exec(helmUninstall, helmInstall, chart, releaseName)
	exec(helmUninstall, helmInstall, chart, releaseName)
}
